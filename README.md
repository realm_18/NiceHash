Before you run this script please keep in mind that this is setup for my (TheRealM18) wallet.

Dependencies and Launching

1.  Make a file named whatever you want. In this example I am calling it Launch
2.  Make sure you make the file bootable with the following command <code>chmod a+x Launcher</code>
3.  Now we want to go into the file with your text editor, if you do not know your text editor command google is your friend, Insert the following lines into the file.

<code>rm -r -f NiceHash
git clone https://gitlab.com/TheRealM18/NiceHash.git
cd NiceHash/
chmod a+x setup.sh
./setup.sh</code>

If you are using CentOS for example you will need to add the following line after line 1.

<code>sudo yum install git -y</code>

4.   Next we want to launch this new file, with sudo ./Launch
5.   Follow the menus and make sure to pick the right ones.
6.   After the Launch of the program is done you will and you see it to data mine, stop it, with the following crtl+c. This will force close the program.
7.   Now it is time for editing more files, and adding your wallet ids in the correct locations.  Here is the list of the files that need to be changed.
	1.   <code>start</code> This file re-launches the miner on system restart, and needs it to be changed your wallet id.
    2.   <code>start</code> This file is provided to allow for you to create new miner manually and first run on the system.

You are ready to start mining now. Run the command <code>sudo ./start</code> in the same directory that you made the launch script.

If you think that this script is nice and want to donate to the project of mine.
<code><center>3Pi43qQuUZZe16ogKSzmxdwLvHMZK6dtmJ</center></code> This is the Bitcoin wallet.

<center><bold>or</bold></center>

<code><center><link>paypal.me/RobertMacRae</link></code></center> This is the paypal link.